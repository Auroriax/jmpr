# JMPR by @Auroriax [(auroriax.com)](http://auroriax.com) #

![gbjam5-thumb.gif](https://bitbucket.org/repo/REA7Xr/images/3316820469-gbjam5-thumb.gif)

JMPR is the game I made during GBJAM5, where you had to make a Game Boy-inspired game within a week. I came up with a Zelda-like, boiling it down to the core, by having only jumping and double jumping available for movement. [You can play it on itch.io.](https://auroriax.itch.io/jmpr)

**The project will no longer be actively maintained, and the source code is primarily available for documentation & archiving purposes. You can open this project with Game Maker Studio 1.4, [which you can obtain here.](http://www.yoyogames.com/get)**

## Licence ##
Copyright (c) 2013-2017, Tom 'Auroriax' Hermans

Permission is hereby granted, free of charge, to any person (the "Person") obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software, including the rights to use, copy, modify, merge, publish, distribute the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

1. The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

2. Under no circumstances shall the Person be permitted, allowed or authorized to commercially exploit the Software.

3. Changes made to the original Software shall be labeled, demarcated or otherwise identified and attributed to the Person.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.